<?php
/**
 * Created by PhpStorm.
 * User: Антон
 * Date: 17.01.2018
 * Time: 3:09
 */

require_once 'application/modules/safemysql/safemysql.class.php';
require_once 'application/modules/helpersclass/taskHelpers.php';
require_once 'core/model.php';
require_once 'core/view.php';
require_once 'core/controller.php';
require_once 'core/route.php';

Route::start();