<h1>Js Task</h1>
<script src="/js/names.js"></script>
<script src="https://ajax.googleapis.com/ajax/libs/angularjs/1.6.4/angular.min.js"></script>

<script>
    //корзина уже встроена в ангуляр но я уже начал писать свой вариант ('ngCart')
    var app = angular.module("myShoppingList", []);
    //Фильтр товаров по группе
    app.filter('inGroup', function() {
        return function(items, optional1) {
            var filterResult = [];

            if (typeof items !== 'undefined'){
                for (var i = 0; i < items.length; i++) {
                    var item = items[i];
                    if (item.G == optional1) {
                        filterResult.push(item);
                    }
                }
                return filterResult;
            }
        }
    });
    //валидация количества товара в корзине
    app.directive('countDirective', function() {
        return {
            require: 'ngModel',
            link: function(scope, element, attr, mCtrl) {
                function myValidation(value) {
                    var i = element["0"].attributes["0"].nodeValue;
                        if (value <= scope.productsCar[i].P) {
                            scope.productsCar[i].Pl = value;
                            mCtrl.$setValidity('counVal', true);
                            return value;
                        } else {
                            mCtrl.$setValidity('counVal', false);
                            return scope.productsCar[i].P;
                        }
                }
                mCtrl.$parsers.push(myValidation);
            }
        };
    });
    //контролел приложения
    app.controller("myCtrl", function($scope, $http, $timeout) {
        $scope.currency = 56;
        $scope.counter = 15;
        $scope.color = '';
        //таймер обнавления валюты
        $scope.onTimeout = function(){
            if ($scope.counter > 0) {
                $scope.counter--;
            } else {
                $scope.$apply(function() {
                    $scope.currency = (Math.random() * (60) + 20).toFixed(2);
                    $http.get("js/data.json")
                        .then(function(response) {
                            for (i = 0; i < response.data.Value.Goods.length; i++){
                                if ($scope.productsgroup.indexOf(response.data.Value.Goods[i].G) == -1)
                                //сохраняю группы которые в последстви буду выводить
                                    $scope.productsgroup.push(response.data.Value.Goods[i].G);
                            }
                            //сохраняю список товаров из файла
                            $scope.prodmodel = response.data.Value.Goods;
                        }); 
                });
                $scope.counter = 15;
            }
            mytimeout = $timeout($scope.onTimeout, 1000);
        }
        var mytimeout = $timeout($scope.onTimeout,1000);

        $scope.productsCar = [];
        $scope.productsgroup = [];
        //сохраняю объект имен
        $scope.storemodel =  store;
        //тут получаю json из файла
        $http.get("js/data.json")
            .then(function(response) {
                for (i = 0; i < response.data.Value.Goods.length; i++){
                    if ($scope.productsgroup.indexOf(response.data.Value.Goods[i].G) == -1)
                        //сохраняю группы которые в последстви буду выводить
                        $scope.productsgroup.push(response.data.Value.Goods[i].G);
                }
                //сохраняю список товаров из файла
                $scope.prodmodel = response.data.Value.Goods;
            });
        //добавление в корзину
        $scope.addItem = function (id) {
            for (var i = 0; i < $scope.prodmodel.length; i++) {
                var item = $scope.prodmodel[i];
                if (item.T == id) {
                    if (item.Pl<=item.P) item.Pl = 1;
                    $scope.productsCar.push(item);
                }
            }
        }
        //удаление из корзины
        $scope.removeItem = function (x) {
            $scope.errortext = "";
            $scope.productsCar.splice(x, 1);
        }
        //получаю сумму
        $scope.getSumm = function() {
            var total = 0;
            angular.forEach($scope.productsCar, function(item) {
                total += item.Pl * item.C * $scope.currency;
            });
            return total;
        };
        //отслеживание слушатель функция для курса валют
        $scope.$watch('currency', function(newValue, oldValue) {
            $scope.color = newValue>oldValue?'gren':'red';
        });
    });
</script>

<div ng-app="myShoppingList" ng-controller="myCtrl">
    <p>Time: {{counter}}</p>
    <p>Enter currency value:
        <input  name="countInput" type="text" ng-model="currency">  </p>
    <p>Goods</p>
    <!-- неверная логика, грамотно будет бегать по списку товаров для того что бы выстраивать его
    а уже для групп товаров тащить категори а не наоборот если останется время !отрефакторить! -->
    <div  id="work_area" class="container d-flex flex-wrap">
        <div ng-repeat="g in productsgroup" class="goods  rounded-top container" id='cat-{{g}}'>
            <div class='cat-title row rounded-top'>{{storemodel[g].G}}</div>
            <!-- тут фильтр по группам -->
            <div ng-repeat="p in prodmodel | inGroup : g ">
                <div class="row border">
                    <div class="col-sm-8 goods-name" ng-click="addItem(p.T)">{{storemodel[p.G].B[p.T].N}} ({{p.P}})</div>
                    <div class="col-sm-4 {{color}}">{{p.C * currency | currency : 'RUB ' }}</div>
                </div>
            </div>
        </div>

    </div>
    <hr/>
    <p>Shopping List</p>
    <table class="table">
        <thead>
        <tr>
            <th scope="col">Name</th>
            <th scope="col">Count</th>
            <th scope="col">Price</th>
            <th scope="col"></th>
        </tr>
        </thead>
        <tbody>
        <tr ng-repeat="x in productsCar">
            <td>{{storemodel[x.G].B[x.T].N}}</td>
            <td>
                <form name="countForm">
                <div class="form-group row {{countForm.countInput.$valid?'':'has-error'}}">
                    <input data="{{$index}}" name="countInput" class="form-control col-sm-2" type="text" ng-model="x.Pl" count-directive> <div class="labelIn"> /pc.</div>
                </div>
                </form>
            </td>
            <td class="{{color}}"><strong>{{x.Pl * x.C * currency | currency : 'RUB ' }}</strong> /pc.</td>
            <td><span ng-click="removeItem($index)">Delete</span></td>
        </tr}
        </tbody>
    </table>
    <p><strong>Total: <span class="{{color}}">{{getSumm() | currency : 'RUB '}}</span></strong></p>
</div>

<p><a href="/"><< Home</a></p>
