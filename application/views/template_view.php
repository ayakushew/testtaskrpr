<!DOCTYPE html>
<html lang="ru">
<head>
    <meta charset="utf-8">
    <title>My test task project</title>
    <script src="/js/jquery/jquery-3.2.1.min.js"></script>
    <link href="css/style.css" rel="stylesheet">
    <link href="css/bootstrap/css/bootstrap.min.css" rel="stylesheet">
</head>
<body>

<?php include 'application/views/'.$content_view; ?>
</body>
</html>