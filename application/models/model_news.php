<?php
/**
 * Created by PhpStorm.
 * User: Антон
 * Date: 17.01.2018
 * Time: 4:00
 */

class Model_News extends Model
{
    public function get_data()
    {
        $news = array();
        return $news;
    }

    public function getAllNews(){
        $news = array();
        $db = new SafeMySQL(array('db' => 'test_task'));
        $news = $db->getAll("SELECT * FROM news");

        return $news;
    }

    public function getNewsById($id){
        $news = array();
        $db = new SafeMySQL(array('db' => 'test_task'));
        $news = $db->getAll('SELECT * FROM news WHERE id = ?i',$id);

        return $news;
    }
}