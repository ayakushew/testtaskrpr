<?php
/**
 * Created by PhpStorm.
 * User: Антон
 * Date: 17.01.2018
 * Time: 3:22
 */

include 'application/modules/simplehtmldom/simple_html_dom.php';

class Controller_Parse extends Controller
{
    function getHtmlView($url, $elem){
        $html = file_get_html($url,0,null,3321,1024);
        $view = $html->find($elem);

        return $view;
    }

    //парсер через стандортную библиотеку PHP Simple HTML DOM Parser
    function action_index()
    {
        //Подключение к БД u - 'root', p - ''
        $db = new SafeMySQL(array('db' => 'test_task'));

        $i = 0;
        $msg = '';
        $news = $this->getHtmlView('https://www.rbc.ru', 'div.news-feed__list a');

        foreach($news as $element) {
            if($i==15) break;
            $tmp = array();

            $tmpHref = $element->attr["href"];
            $urlArr = explode('/', $element->attr["href"]);

            if (!array_search('www.rbc.ru', $urlArr)) continue;

            $tmp["category"]=($urlArr[2]=='www.rbc.ru')?$urlArr[3]:'';
            $tmp["title"] = trim($element->children[0]->plaintext);

            $dataSource = explode(',', $element->children[1]->children[0]->plaintext);
            //Source - $dataSource[1]

            if(count($dataSource)==2)
                $tmp["data"] = date("Y-m-d").' '.$dataSource[0].':00';
            else {
                $day = explode(' ', $dataSource[0]);
                $tmp["data"] = date("Y-m").'-'.$day[0].' '.$dataSource[1].':00';
            }

            $newDetail = $this->getHtmlView($tmpHref,'div.article__text');
            $img = $this->getHtmlView($tmpHref, 'div.article__text div.article__main-image__link img');

            $tmp["img"] = ($img[0]->attr['src']!=NULL)?$img[0]->attr['src']:'';
            $tmp["description"] = htmlspecialchars($newDetail[0]->innertext);
            $tmp["short_description"] = mb_strimwidth(trim(preg_replace("/  +/"," ",$newDetail[0]->plaintext)), 0, 200, "...");

            $sql = 'SELECT id FROM news WHERE title = ?s';
            $result = $db->query($sql, $tmp["title"]);
            if ($result) {$msg = 'Add '.$i.' news.'; break;}

            $sql = 'INSERT INTO news SET ?u';
            $result = $db->query($sql, $tmp);

            if (!$result){$message = 'Error'; break;}
            $i++;
        }
        if ($result) $msg .= "\n".'done!';

        unset($db);
        $msg[0] = 'asdf';
        $this->view->generate('parse_view.php', 'template_view.php', $msg);
    }
}