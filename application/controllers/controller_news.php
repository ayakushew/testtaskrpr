<?php
/**
 * Created by PhpStorm.
 * User: Антон
 * Date: 17.01.2018
 * Time: 3:22
 */

class Controller_News extends Controller
{
    function __construct()
    {
        $this->model = new Model_News();
        $this->view = new View();
    }

    function action_index()
    {
        $data = $this->model->getAllNews();
        $this->view->generate('news_view.php', 'template_view.php', $data);
    }

    function action_detail($item_id)
    {
        if(isset($item_id)) {
            $data = $this->model->getNewsById($item_id);

            $this->view->generate('newsdetail_view.php', 'template_view.php', $data[0]);
        } else Route::ErrorPage404();
    }
}