//получение json'а из файла
function getData( url, ready ) {
    var xhr = new XMLHttpRequest();
    xhr.open( 'GET', url, true );
    xhr.onreadystatechange = function() {
        if( this.readyState === 4 && this.status !== 404 ) {
            ready( this.responseText );
        }
    }
    xhr.send();
}

$(function() {

    getData( 'js/data.json', function( jsonData ) {
        jsonData = JSON.parse(jsonData);
        console.log( jsonData.Value.Goods );
        var work_area =  $('#work_area');
        console.log( work_area);

        //console.log( product, jsonData.Value.Goods.length );
        //"C" - цена в долларах(USD) - вывести в рублях(курс выбрать произвольно)
        //"G" - id группы
        // "T" - id товара
        // "P" - сколько единиц товара осталось
        var i;
        for (i = 0; i < jsonData.Value.Goods.length; i++) {
            var product = jsonData.Value.Goods[i];
            //console.log(store);
            if (!$("div").is('#cat-' + product.G)){
                work_area.append(
                    "<div class=\"goods  rounded-top container\" id='cat-" + product.G + "'>" +
                    "<div class='cat-title row rounded-top'>" + store[product.G].G + "</div>" +
                    "</div>");
            }
            $('#cat-' + product.G).append(
                "<div class=\"row border\">" +
                "<div class=\"col-sm-8 goods-name\" ng-click=\"addItem("+product.T+")\">" + store[product.G].B[product.T].N + " (" + product.P + ") </div>" +
                "<div class=\"col-sm-4\">" + product.C + "</div>" +
                "</div>");
        }
    });

});l


